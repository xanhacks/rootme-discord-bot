FROM python:3.9.1-alpine

WORKDIR /usr/src/app/

RUN apk add --no-cache gcc libc-dev
RUN echo "00 * * * * /usr/local/bin/python3 /usr/src/app/refresh_db.py" > /etc/crontabs/bot
RUN chmod 0644 /etc/crontabs/bot
RUN crond -b

COPY . .
RUN adduser --disabled-password bot
RUN chown -R bot:bot /usr/src/app/

USER bot
RUN pip install --no-warn-script-location --no-cache-dir -r requirements.txt

CMD ["python", "./run.py"]
