# RootMe Discord Bot

This bot is made with python and the discord.py module.

## Features

- Retrieve the score of a user
- Create your own leaderboard
- Random hacker gif (require Giphy API KEY)
- Clear the chat
- Multi servers support (thanks [Ooggle](https://gitlab.com/Ooggle))

## Setup

### Classic way

1. Clone the repository
2. Copy app/.env.sample to app/.env
3. Fill the .env file with our discord bot token (optional : Giphy API KEY)
4. Install dependencies

```shell
python3 -m pip install -r requirements.txt
```

5. Run the bot

```shell
python3 run.py
```

6. Setup crontab (optional)

```shell
00 * * * * cd /home/user/rootme-discord-bot/ && /usr/bin/python3 refresh_db.py
```

### With docker

1. Clone the repository
2. Copy app/.env.sample to app/.env
3. Fill the .env file with our discord bot token (optional : Giphy API KEY)
4. Build and run the docker

```shell
docker build . -t dockerbot
docker run -it dockerbot
```

### With docker-compose (persistent database)

1. Clone the repository
2. Copy app/.env.sample to app/.env
3. Fill the .env file with our discord bot token (optional : Giphy API KEY)
4. run the docker-compose file

```shell
docker-compose up -d
```

## Docs

### Reload leaderboard

The python script `refresh_db.py` allows you to refresh all the database, you can use a `crontab` to refresh the leaderboard daily (the crontab is already setup inside the docker installation). If you want to refresh only one user, you can do the `!rootme <name>` command. Rootme uses its own cache, so the score can take one day to be updated.

### Permission

You can find a list of all the discord permissions, [here](https://discordpy.readthedocs.io/en/latest/api.html#permissions).

```
!clear -> view_audit_log
!adminrootme -> view_audit_log
```

### clear chat

```
!clear <nb_message_to_clear>
```

### Example 1

![example_1](assets/commands_example_1.png)

### Example 2

![example_2](assets/commands_example_2.png)
