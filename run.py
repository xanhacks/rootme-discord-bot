#!/usr/bin/env python3
import time
from os import getenv
from os.path import isfile
import discord
from discord.ext import commands
import asyncio
from dotenv import load_dotenv, find_dotenv
from utils import database, parser

client = commands.Bot(command_prefix="!")
client.remove_command("help")


@client.event
async def on_ready():
    print(f"[{time.strftime('%d/%m %H:%M')}] BOT is ready !")


@client.command()
async def help(ctx):
    embed = discord.Embed(title="Help message", color=0xF0932B)
    embed.add_field(name="!rootme help", value="Display this message", inline=False)
    embed.add_field(
        name="!rootme <username>",
        value="Display the score of a user",
        inline=False,
    )
    embed.add_field(
        name="!leaderboard", value="Display the rootme leader board", inline=False
    )
    embed.add_field(name="!hacker", value="Display a random gif hacker", inline=False)
    await ctx.send(embed=embed)


@client.command()
async def rootme(ctx, username=None):
    if username == "help":
        embed = discord.Embed(title="Help message", color=0xF0932B)
        embed.add_field(
            name="!rootme help",
            value="Display this message",
            inline=False,
        )
        embed.add_field(
            name="!rootme <username>",
            value="Display the score of a user",
            inline=False,
        )
        embed.add_field(
            name="!leaderboard", value="Display the rootme leader board", inline=False
        )
        embed.add_field(
            name="!hacker", value="Display a random gif hacker", inline=False
        )
        await ctx.send(embed=embed)
        return

    if not username:
        await ctx.send(f"Use: !rootme <username>")
        return

    await ctx.send(f"Searching score for: {username}")
    score = parser.get_score(username)

    if score:
        await ctx.send(f"Score: {score}")
        DB.update_score(ctx.guild.id, username.lower(), score)
    else:
        await ctx.send(f"Unable to find the score for: {username}")


@client.command()
async def hacker(ctx):
    await ctx.send(parser.hacker_gif_link())


@client.command(pass_context=True)
@commands.has_permissions(view_audit_log=True)
async def clear(ctx, number=None):
    if not number or not number.isdigit():
        await ctx.send(f"Use: !clear <number>")
        return

    number = int(number)
    if number > 0:
        async for x in ctx.message.channel.history(limit=number + 1):
            await x.delete()
    else:
        await ctx.send(f"Use: !clear <number>")


@client.command()
@commands.has_permissions(view_audit_log=True)
async def adminrootme(ctx, command=None, username=None, discord_name=None, score=None):
    if not command or command == "help":
        embed = discord.Embed(title="Help message", color=0xF0932B)
        embed.add_field(
            name="!adminrootme help", value="Display this message", inline=False
        )
        embed.add_field(
            name="!adminrootme adduser <rootme_username> <discord_name> <score>",
            value="Add a user in the database",
            inline=False,
        )
        embed.add_field(
            name="!adminrootme remove <rootme_username>",
            value="Remove a user from the database",
            inline=False,
        )
        embed.add_field(
            name="!adminrootme show", value="Show the whole database", inline=False
        )
        await ctx.send(embed=embed)
        return
    if command == "adduser":
        if score:
            DB.add_user(ctx.guild.id, discord_name, username, score)
            await ctx.send(f"{username} added to the database !")
        else:
            await ctx.send(f"Use '!adminrootme help' !")
    elif command == "remove":
        if username:
            DB.remove_user(ctx.guild.id, username)
            await ctx.send(f"{username} removed from the database !")
        else:
            await ctx.send(f"Use '!adminrootme help' !")
    elif command == "show":
        users = DB.show_db(ctx.guild.id)

        embed = discord.Embed(title="Show database", color=0xF0932B)
        for row in users:
            embed.add_field(name=row, value="from rootme table", inline=False)
        await ctx.send(embed=embed)
    elif command == "drop":
        DB.drop_table()
        DB.create_table()
        await ctx.send("Table drop")


@client.command()
async def leaderboard(ctx):
    users = DB.show_leader_board(ctx.guild.id)

    embed = discord.Embed(title="RootMe - Leader board", color=0xFFFFFF)
    for count, row in enumerate(users):
        embed.add_field(
            name=f"Discord: {row[0]}",
            value=f"#{count + 1} @{row[1]} : {row[2]} points",
            inline=True,
        )
    await ctx.send(embed=embed)


if __name__ == "__main__":
    if not isfile(".env"):
        print("Please create a .env file")
        exit(0)

    load_dotenv(".env")
    discord_token = getenv("DISCORD_BOT_TOKEN")
    
    DB = database.Database()
    
    try:
        client.run(discord_token)
    except discord.errors.HTTPException and discord.errors.LoginFailure:
        print("Login failure, check your discord token !")
        exit(0)

    DB.exit_db()
