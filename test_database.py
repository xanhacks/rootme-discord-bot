#!/usr/bin/env python3
import unittest
from utils.database import Database

class Test_Database(unittest.TestCase):

    def setUp(self):
        self.db = Database(table_name="rootme", connect_string=":memory:")
        self.data = [
            {"guild_id": 1, "discord_name": "test1", "username": "xanhacks", "score": 100},
            {"guild_id": 1, "discord_name": "test2", "username": "xanhacks2", "score": 200},
            {"guild_id": 2, "discord_name": "test3", "username": "xanhacks", "score": 300}
        ]

        # insert dummy data
        for row in self.data:
            self.db.add_user(row["guild_id"], row["discord_name"], row["username"], row["score"])

    def tearDown(self):
        self.db.exit_db()

    def test_insert(self):
        # check database count
        self.assertEqual(self.db.get_count(), len(self.data))
        
        # check usernames
        cursor = self.db.get_all_user()
        for i, row in enumerate(cursor):
            self.assertEqual(row[0], self.data[i]["username"])
        
        # check leaderboard order
        leaderboard = [l for l in self.db.show_leader_board(guild_id=1)]
        self.assertEqual(leaderboard[0], (self.data[1]["discord_name"],\
            self.data[1]["username"], self.data[1]["score"]))
        self.assertEqual(leaderboard[1], (self.data[0]["discord_name"],\
            self.data[0]["username"], self.data[0]["score"]))

        leaderboard = [l for l in self.db.show_leader_board(guild_id=2)]
        self.assertEqual(leaderboard[0], (self.data[2]["discord_name"],\
            self.data[2]["username"], self.data[2]["score"]))

    def test_delete(self):
        # check initial data count
        self.assertEqual(self.db.get_count(), len(self.data))
        
        leaderboard = [l for l in self.db.show_leader_board(guild_id=1)]
        self.assertEqual(len(leaderboard), 2)

        leaderboard = [l for l in self.db.show_leader_board(guild_id=2)]
        self.assertEqual(len(leaderboard), 1)

        # remove third user
        self.db.remove_user(username=self.data[2]["username"], guild_id=self.data[2]["guild_id"])
        self.assertEqual(self.db.get_count(), len(self.data)-1)
        leaderboard = [l for l in self.db.show_leader_board(guild_id=self.data[2]["guild_id"])]
        self.assertEqual(len(leaderboard), 0)

        # remove first user
        self.db.remove_user(username=self.data[0]["username"], guild_id=self.data[0]["guild_id"])
        self.assertEqual(self.db.get_count(), len(self.data)-2)
        leaderboard = [l for l in self.db.show_leader_board(guild_id=self.data[0]["guild_id"])]
        self.assertEqual(len(leaderboard), 1)

        # remove second user
        self.db.remove_user(username=self.data[1]["username"], guild_id=self.data[1]["guild_id"])
        self.assertEqual(self.db.get_count(), len(self.data)-3)
        leaderboard = [l for l in self.db.show_leader_board(guild_id=self.data[1]["guild_id"])]
        self.assertEqual(len(leaderboard), 0)

    def test_get_all_user(self):
        usernames_db = [c[0] for c in self.db.get_all_user()]
        usernames_data = set([row["username"] for row in self.data])

        # check same length
        self.assertEqual(len(usernames_db), len(usernames_data))

        # compare usernames
        for username_db, username_data in zip(set(usernames_db), usernames_data):
            self.assertEqual(username_db, username_data)

if __name__ == '__main__':
    unittest.main()