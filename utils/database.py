#!/usr/bin/env python3
import sqlite3
from time import sleep
from os.path import isfile
from utils import parser


class Database:
    def __init__(self, table_name="rootme", connect_string="database/rootme.db"):
        self.table_name = table_name

        self.con = sqlite3.connect(connect_string)
        self.c = self.con.cursor()
        self.create_table()

    def create_table(self):
        """ Create the table """
        query = f"CREATE TABLE {self.table_name} (id INTEGER PRIMARY KEY AUTOINCREMENT,\
         guild_id INTEGER, discord_name VARCHAR (50), username VARCHAR (30), score INTEGER);"

        try:
            self.c.execute(query)
            self.con.commit()
        except sqlite3.OperationalError:
            print(f"The table '{self.table_name}' is already created.")

    def add_user(self, guild_id, discord_name, username, score):
        query = f"INSERT INTO {self.table_name} (guild_id, discord_name, username, score) VALUES (?, ?, ?, ?);"

        self.c.execute(query, (guild_id, discord_name, username.lower(), score))
        self.con.commit()

    def remove_user(self, guild_id, username):
        query = (
            f"DELETE FROM {self.table_name} WHERE username == ? AND guild_id = ?;"
        )

        self.c.execute(query, (username.lower(), guild_id))
        self.con.commit()

    def update_score(self, username, score):
        query = f"UPDATE {self.table_name} SET score = ? WHERE username = ?';"

        self.c.execute(query, (score, username.lower()))
        self.con.commit()

    def show_leader_board(self, guild_id):
        query = f"SELECT discord_name, username, score FROM {self.table_name} WHERE guild_id = ? ORDER BY score DESC;"

        self.c.execute(query, (guild_id,))
        self.con.commit()

        return self.c

    def show_db(self, guild_id):
        query = f"SELECT * FROM {self.table_name} WHERE guild_id = ?;"

        self.c.execute(query, (guild_id,))
        self.con.commit()

        return self.c

    def drop_table(self):
        query = f"DROP TABLE {self.table_name};"

        self.c.execute(query)
        self.con.commit()

    def get_all_user(self):
        query = f"SELECT DISTINCT username FROM {self.table_name};"
        self.c.execute(query)
        self.con.commit()

        return self.c

    def get_count(self):
        query = f"SELECT COUNT(*) FROM {self.table_name};"

        self.c.execute(query)
        self.con.commit()

        return self.c.fetchone()[0]

    def refresh(self):
        usernames = [c[0] for c in self.get_all_user()]

        for username in usernames:
            print(f"Searching score for: {username}")
            score = parser.get_score(username)

            if score:
                print(f"{username} -> {score}")
                self.update_score(username.lower(), score)
            else:
                print(f"Unable to find the score for: {username}")

            sleep(5)

    def exit_db(self):
        self.con.close()
