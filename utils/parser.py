#!/usr/bin/env python3
from os import getenv
from random import choice
from json import loads
from requests import get
from bs4 import BeautifulSoup

def check_username(username):
    return True if isinstance(username, str) and username.isalnum() else False


def check_uuid(uuid):
    return True if isinstance(uuid, int) else False


def get_score(username):
    if check_username(username):
        url = f"https://www.root-me.org/{username}?inc=score"

        req = get(
            url,
            headers={
                "User-agent": "Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0"
            },
        )
        req.close()
        soup = BeautifulSoup(req.text, "html.parser").find_all("h3")

        correct_index = 0
        for index, elem in enumerate(soup):
            if "valid.svg" in str(elem):
                correct_index = index
                break

        if len(soup) > 4:
            score = soup[correct_index].text.strip()
            if score.isdigit():
                return int(score)
    return None


def get_rootme_uuid(username):
    if check_username(username):
        req_text = get(
            "https://www.root-me.org/" + username,
            headers={
                "User-agent": "Mozilla/5.0 (X11; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0"
            },
        ).text
        return int(
            req_text.split(
                "<div class='notation_note' ><div class='star-rating ratingstar_group_notation-auteur"
            )[1].split("-")[0]
        )
    return None


def get_score_api(uuid):
    if check_uuid(uuid):
        rootme_api_key = getenv("ROOTME_API_KEY")

        req = get(
            f"https://api.www.root-me.org/auteurs/{uuid}",
            cookies={"api_key": rootme_api_key},
        )

        if req.status_code == 200:
            return int(req.json()["score"])
        elif req.status_code == 429:
            print("Response from Rootme API : Too Many Requests.")
    return None


def hacker_gif_link(query="hacker"):
    default_gif = "https://media1.giphy.com/media/DBfYJqH5AokgM/200.mp4?cid=790b76114562f78e2d00c0a8692a7bc9a474889235ebd53c"
    api_key = getenv("GIPHY_API_KEY")

    if api_key:
        url = (
            f"https://api.giphy.com/v1/gifs/search?api_key={api_key}&q={query}&limit=50"
        )
        try:
            req = get(url)
            json_ouput = loads(req.text)

            if "data" in json_ouput:
                data = json_ouput["data"]

                urls = []
                for gif in data:
                    urls.append(gif["url"])

                return choice(urls)
        except ConnectionError:
            print("Error : Unable to connect to https://api.giphy.com")

    return default_gif